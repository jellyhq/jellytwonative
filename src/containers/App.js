import React, { Component, StyleSheet } from 'react-native';
import { NavigatorIOS } from 'react-native';
import Feed from './Feed';

const initialRoute = {
  component: Feed,
  title: 'Jelly',
  passProps: {}
};

export default class App extends Component {
  render() {
    return (
      <NavigatorIOS
        style={styles.wrapper}
        barTintColor="#2EBAA3"
        titleTextColor="#FFFFFF"
        initialRoute={initialRoute} />
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1
  }
});
