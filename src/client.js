import React, { Component } from 'react-native';
import { Provider } from 'react-redux/native';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import asyncPromiseMiddleware from 'jelly-two-admin/src/scripts/redux/asyncPromiseMiddleware.js';
import apiClient from 'jelly-two-admin/src/scripts/api/client.js';
import * as reducers from 'jelly-two-admin/src/scripts/ducks';
import App from './containers/App';

var client = new apiClient();
var middleware = applyMiddleware(thunkMiddleware, asyncPromiseMiddleware(client));
var finalCreateStore = middleware(createStore);
var reducer = combineReducers(reducers);
var store = finalCreateStore(reducer, {});

export default class Client extends Component {
  render() {
    return (
      <Provider store={store}>
        {() => <App />}
      </Provider>
    );
  }
}
