# Jelly Two React Native

## Some hacks

In order to use some ES6/ES7 features (and thus interoperability between native and web javascript
code) we need to slightly hack the packager.

https://github.com/facebook/react-native/issues/1480#issuecomment-121649714

## Shared code

The goal is to share as much code between the web app and native apps. Currently I'm linking the
jelly-two-admin repository via `npm install ../jelly-two-admin`. You will need to pull that repo
as a sibling to this one.