import React, { Component, PropTypes } from 'react-native';
import { View, TextInput } from 'react-native';

export default class AutogrowTextInput extends Component {
  static propTypes = {
    maxHeight: PropTypes.number.isRequired,
    ...TextInput.propTypes
  }

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ''
    };
  }

  render() {
    return (
      <TextInput {...this.props}
        multiline={true}
        value={this.state.value}
        onChangeText={(text) => this._onChangeText(text)}/>
    );
  }

  _onChangeText(val) {
    this.setState({
      value: val
    });

    this.props.onChangeText && this.props.onChangeText(val);
  }
}

