import React, { PropTypes, Component, StyleSheet } from 'react-native';
import {
  View, Text, TouchableOpacity
} from 'react-native';

export default class QuestionListRow extends Component {
  static propTypes = {
    question: PropTypes.object.isRequired,
    onPress: PropTypes.func.isRequired
  }

  render() {
    const { question } = this.props;
    return (
      <TouchableOpacity onPress={this.props.onPress}>
      <View>
          <View style={styles.rowStyle}>
              <Text style={styles.rowText}>{question.text}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

var styles = StyleSheet.create({
  rowStyle: {
    paddingVertical: 20,
    paddingLeft: 16,
    borderTopColor: 'white',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderBottomColor: '#E0E0E0',
    borderWidth: 1
  },
  rowText: {
    color: '#212121',
    fontSize: 16
  }
});
