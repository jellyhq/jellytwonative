import React, { Component, PropTypes } from 'react-native';
import {
  View, ListView, Text, StyleSheet
} from 'react-native';
import { connect } from 'react-redux/native';
import { create as createList } from 'jelly-two-admin/src/scripts/utils/pagination';
import { loadFeed, loadDetails } from 'jelly-two-admin/src/scripts/ducks/feed';
import QuestionListRow from '../components/QuestionListRow';
import QuestionDetail from './QuestionDetail';

@connect((state, props) => {
  const feedId = 'browse';
  const list = state.feeds[feedId] || createList();
  const items = list.ids;

  return {
    items: items.map(hash => state.items[hash]),
    questions: state.questions,
    users: state.users
  };
})
class Feed extends Component {
  static propTypes = {
    items: PropTypes.array.isRequired,
    questions: PropTypes.object.isRequired,
    users: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);
    
    // https://facebook.github.io/react-native/docs/listview.html
    const listDataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: listDataSource.cloneWithRows(props.items)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.items !== this.props.items) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(nextProps.items)
      });
    }
  }

  render() {
    console.log('render', this.props.items);
    return (
      <View style={styles.container}>
        {this.props.items.length > 0 ? this._renderList() : null}
      </View>
    );
  }

  _renderList() {
    return (
      <ListView
        style={styles.listView}
        dataSource={this.state.dataSource}
        renderRow={this._renderRow.bind(this)}
      />
    );
  }
  _renderRow(rowData, sectionId, rowId) {
    const question = this.props.questions[rowData.question];
    const onPress = () => this._onPress(rowData, sectionId, rowId);
    return <QuestionListRow question={question} onPress={onPress}/>;
  }

  _onPress(rowData, sectionId, rowId) {
    const { navigator } = this.props;
    const question = this.props.questions[rowData.question];

    // TODO: Don't like that this happens here, but maybe it's okay.
    // Eventually navigation should be handled elsewhere, but the iOS paradigms
    // tend to force this kind of coupling.
    navigator.push({
      title: question.text,
      component: QuestionDetail,
      passProps: {
        dispatch: this.props.dispatch, // ew
        hash: question.hash
      }
    })
  }

  componentDidMount() {
    const { dispatch } = this.props;
    return dispatch(loadFeed('browse'));
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3F51B5',
    flexDirection: 'column',
    paddingTop: 25
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white'
  }
});

export default Feed;
