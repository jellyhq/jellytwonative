import React, { Component, PropTypes, StyleSheet } from 'react-native';
import { ListView, View, Text, TextInput, TouchableHighlight } from 'react-native';
import { loadDetails } from 'jelly-two-admin/src/scripts/ducks/feed';
import { answer } from 'jelly-two-admin/src/scripts/ducks/questions';
import { connect } from 'react-redux/native';
import AutogrowTextInput from '../components/AutogrowTextInput';

const createDataBlob = (question, answers) => {
  return {
    question: [question],
    answers
  };
}

const getSectionData = (dataBlob, section) => {
  return dataBlob[section];
}

const getRowData = (dataBlob, section, row) => {
  const data = dataBlob[section];
  switch (section) {
    case 'question':
      return data[row];
    case 'answers':
      return data[row];
  }

  return null;
}

@connect((state, props) => {
  const question = state.questions[props.hash];
  const answers = question.answers ? question.answers.map(answer => state.answers[answer]) : [];
  return {
    question,
    answers
  };
})
export default class QuestionDetail extends Component {
  static propTypes = {
    hash: PropTypes.string.isRequired,
    question: PropTypes.object.isRequired,
    answers: PropTypes.array
  }

  constructor(props) {
    super(props);

    const dataSource = new ListView.DataSource({
      getRowData,
      getSectionData,
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    });

    const blob = createDataBlob(this.props.question, this.props.answers || []);
    this.state = {
      dataSource: dataSource.cloneWithRowsAndSections(blob, ['question', 'answers']),
      answerInputValue: 'Hey bob!'
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.answers !== this.props.answers) {
      const blob = createDataBlob(nextProps.question, nextProps.answers || []);
      this.setState({
        dataSource: this.state.dataSource.cloneWithRowsAndSections(blob, ['question', 'answers'])
      });
    }
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <ListView 
          style={styles.listView}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow.bind(this)}
        />
        <View style={styles.replyContainer}>
          <View style={styles.replyLeft}>
            <AutogrowTextInput 
              value={this.state.answerInputValue}
              style={styles.textInput}
              maxHeight={60}
              onChangeText={(val) => this._onChangeText(val)}/>
          </View>
          <View style={styles.replyRight}>
            <TouchableHighlight onPress={() => this._onSubmitPress()}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }

  _onChangeText(val) {
    this.setState({
      answerInputValue: val
    });
  }

  _renderRow(rowData, sectionId, rowId) {
    switch (sectionId) {
      case 'question':
        return this._renderQuestion(rowData);
      case 'answers':
        return this._renderAnswer(rowData);
    }
  }

  _renderQuestion(question) {
    return (
      <View style={styles.detail}>
        <Text style={styles.detailText}>{question.text}</Text>
      </View>
    );  
  }

  _renderAnswer(answer) {
    return (
      <View style={styles.answer}>
        <Text style={styles.answerText}>{answer.text}</Text>
      </View>
    );
  }

  _onSubmitPress() {
    const { dispatch } = this.props;
    dispatch(answer(this.props.hash, {
      text: this.state.answerInputValue
    }));
  }

  componentDidMount() {
    const { dispatch, hash } = this.props;
    dispatch(loadDetails(hash));
  }
}

const styles = StyleSheet.create({
  replyContainer: {
    flex: 1,
    flexDirection: 'row',
    height: 44 
  },
  replyLeft: {
    flex: 1
  },
  replyRight: {
    width: 100
  },
  textInput: {
    padding: 8,
    height: 52,
    fontSize: 20,
    borderColor: '#000000',
    borderWidth: 1
  },
  submitText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 20
  },
  wrapper: {
    flex: 1,
    paddingTop: 75
  },
  detail: {
    flex: 1,
    padding: 20
  },
  detailText: {
    fontSize: 24
  },
  answer: {
    flex: 1
  }
});

